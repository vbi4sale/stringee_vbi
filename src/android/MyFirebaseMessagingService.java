package com.stringee.cordova;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.content.Context;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.stringee.listener.StatusListener;
//import com.vbi.myvbi.MainActivity;
//import com.vbi.myvbi.R;
//import com.vbi.myvbi.MainActivity;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by luannguyen on 9/5/2017.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private NotificationManager mNotificationManager;
    private NotificationCompat.Builder mBuilder;
    public static final String NOTIFICATION_CHANNEL_ID = "100001";
    @Override
    public void onNewToken(String token) {
        if (Common.client != null) {
            Common.client.registerPushToken(token, new StatusListener() {
                @Override
                public void onSuccess() {

                }
            });
        }
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.d("Stringee", "Push notification message received");
        if (remoteMessage.getData().size() > 0) {
//            openAndcreatedb();
            String pushFromStringee = remoteMessage.getData().get("stringeePushNotification");
            if (pushFromStringee != null) {
                String data = remoteMessage.getData().get("data");
                Log.d("Stringee", data);
                try {
                    JSONObject jsonObject = new JSONObject(data);
                    String callStatus = jsonObject.getString("callStatus");
                    if (callStatus != null && callStatus.equals("started")) {
                        if (Common.client == null || !Common.client.isConnected()) {
                            Class mainActivity = null;
                            Context context = getApplicationContext();
                            String packageName = context.getPackageName();
                            Intent launchIntent = context.getPackageManager().getLaunchIntentForPackage(packageName);
                            String className = launchIntent.getComponent().getClassName();
                            try {
                                //loading the Main Activity to not import it in the plugin
                                mainActivity = Class.forName(className);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            Intent openActivityIntent = new Intent(context, mainActivity);


                            openActivityIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            startActivity(openActivityIntent);
//                            Intent intent = new Intent(this, MainActivity.class);
//                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                            startActivity(intent);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            else{
                String title = remoteMessage.getData().get("title");
                String body = remoteMessage.getData().get("body");
                createNotification(title,body);
            }
        }
    }

    public void createNotification(String title, String message) {
        String packageName = getApplication().getPackageName();
        Class mainActivity = null;
        Intent launchIntent = getApplication().getPackageManager().getLaunchIntentForPackage(packageName);
        String className = launchIntent.getComponent().getClassName();
        try {
            //loading the Main Activity to not import it in the plugin
            mainActivity = Class.forName(className);
        } catch (Exception e) {
            e.printStackTrace();
        }
        /**Creates an explicit intent for an Activity in your app**/
        Intent resultIntent = new Intent(getApplicationContext(), mainActivity);
        resultIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        PendingIntent resultPendingIntent = PendingIntent.getActivity(getApplicationContext(),
                0 /* Request code */, resultIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        mBuilder = new NotificationCompat.Builder(getApplicationContext());
        mBuilder.setSmallIcon(getApplicationInfo().icon);
        mBuilder.setContentTitle(title)
                .setContentText(message)
                .setAutoCancel(false)
                .setSound(Settings.System.DEFAULT_NOTIFICATION_URI)
                .setContentIntent(resultPendingIntent);

        mNotificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "NOTIFICATION_MYVBI", importance);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.enableVibration(true);
            notificationChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
            assert mNotificationManager != null;
            mBuilder.setChannelId(NOTIFICATION_CHANNEL_ID);
            mNotificationManager.createNotificationChannel(notificationChannel);
        }
        assert mNotificationManager != null;
        mNotificationManager.notify(0 /* Request Code */, mBuilder.build());
    }
//   public void openAndcreatedb(){
//       SQLiteDatabase db = openOrCreateDatabase("notiMyVBIDb.db", Context.MODE_PRIVATE, null);
//       String querycreatetbl="CREATE TABLE IF NOT EXISTS myvbinotification ("
//       +"id integer primary key,"+
//               "deviceId text,"+
//               "tokenId text,"+
//               "title text,"+
//               "contentNotification text,"+
//               "typedata text,"+
//               "senddata integer);";
//       db.execSQL(querycreatetbl);
//       String insertdata ="INSERT or replace INTO myvbinotification (deviceId, tokenId, title, contentNotification,typedata,senddata) VALUES('test120','4/2020','5000','tranlong','y','0')";
//       db.execSQL(insertdata);
////       db.close();
//   }
}
