package com.stringee.cordova;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.view.Gravity;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by luannguyen on 7/12/2017.
 */

public class Utils {

    public static void hideKeyboard(Activity activity) {
        try {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_IMPLICIT_ONLY);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void showKeyboard(Context context) {
        ((InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE)).toggleSoftInput(
                InputMethodManager.SHOW_IMPLICIT, InputMethodManager.HIDE_IMPLICIT_ONLY);
    }

    public static void reportMessage(Context context, String message) {
        Toast toast = Toast.makeText(context, message, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, 0);
        TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
        if (v != null)
            v.setGravity(Gravity.CENTER);
        toast.show();
    }

    public static void reportMessage(Context context, int resId) {
        Toast toast = Toast.makeText(context, context.getString(resId), Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, 0);
        TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
        if (v != null)
            v.setGravity(Gravity.CENTER);
        toast.show();
    }

    public static boolean isPhoneNumber(String text) {
        if (text == null || text.length() == 0) {
            return false;
        }
        for (int i = 0; i < text.length(); i++) {
            char c = text.charAt(i);
            if (!((c >= '0' && c <= '9') || c == '+' || c == ' ')) {
                return false;
            }
        }
        return true;
    }

    public static byte[] fileToBytes(File file) {
        try {
            FileInputStream ios = new FileInputStream(file);
            int read = 0;
            byte[] bytes = new byte[(int) file.length()];
            byte[] buffer = new byte[4096];
            int total = 0;
            while ((read = ios.read(buffer)) != -1) {
                System.arraycopy(buffer, 0, bytes, total, read);
                total += read;
            }
            try {
                ios.close();
            } catch (Exception ex) {
            }
            return bytes;
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }


    public static String formatPhone(String phone) {
        if (phone != null) {
            phone = phone.replaceAll("\\s+", "");
            phone = phone.replace("+840", "84");
            phone = phone.replace("+84", "84");
            if (phone.startsWith("0")) {
                phone = "84" + phone.substring(1, phone.length());
            }
        }
        return phone;
    }

    /**
     * Get free call time from duration
     *
     * @param duration
     * @return
     */
    public static String getCallTime(long duration) {
        SimpleDateFormat format = new SimpleDateFormat("mm:ss");
        format.setTimeZone(TimeZone.getTimeZone("GMT"));
        String res = format.format(new Date(duration));
        return res == null ? "00:00" : res;
    }

    /**
     * Get free call time
     *
     * @param currentTime
     * @param startTime
     * @return
     */
    public static String getCallTime(long currentTime, long startTime) {
        long time = currentTime - startTime;
        SimpleDateFormat format = new SimpleDateFormat("mm:ss");
        format.setTimeZone(TimeZone.getTimeZone("GMT"));
        return format.format(new Date(time));
    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetInfo = connectivityManager.getActiveNetworkInfo();
        boolean isConnected = activeNetInfo != null && activeNetInfo.isConnected();
        return isConnected;
    }
}
